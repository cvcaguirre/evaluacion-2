"""RingoPetStore URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from RingoPetStore.views.home import load_home
from RingoPetStore.views.galeria import galeria_index
from RingoPetStore.views.contacto import contacto_index
from RingoPetStore.views.pedidos import pedidos_index
from RingoPetStore.views.contacto import contacto_index
from RingoPetStore.views.api import index


urlpatterns = [
    path('admin/', admin.site.urls),
    path('Inicio.html', load_home),
    path('', load_home),
    path('Inicio.html', index),
    path('', index),
    path('Galeria.html', galeria_index),
    path('Contacto.html', contacto_index),
    path('forms/Pedidos.html', pedidos_index),
    
]
