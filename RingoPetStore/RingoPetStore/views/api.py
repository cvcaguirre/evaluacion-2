from django.shortcuts import render
import requests


def index(request):
    
    api_url = "https://mindicador.cl/api"
    res = requests.get(api_url)
    print('status_code -> {0}'.format(res.status_code))
    json_request = res.json()
    print('json_request -> {0}'.format(json_request))
    dolar = json_request['dolar']['valor']
    fecha = json_request['dolar']['fecha']
    print('Retornar pagina Inicio.html')
    return render(request, 'Inicio.html', {'dolar': dolar, 'fecha': fecha})